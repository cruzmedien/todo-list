import React, { Component } from "react";
import Todo from "./Todo";
import NewTodoForm from "./NewTodoForm";
import "./TodoList.css";

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: []
    };

    // Binding
    this.create = this.create.bind(this);
    this.remove = this.remove.bind(this);
    this.update = this.update.bind(this);
    this.toogleCompletion = this.toogleCompletion.bind(this);
  }

  // Creating a new Todo
  create(newTodo) {
    this.setState({
      todos: [...this.state.todos, newTodo]
    });
  }

  // Deleting a Todo
  remove(id) {
    this.setState({
      todos: this.state.todos.filter(todo => todo.id !== id)
    });
  }

  // Updating the form Elements
  update(id, updatedTask) {
    const updatedTodos = this.state.todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, task: updatedTask };
      }
      return todo;
    });
    this.setState({ todos: updatedTodos });
  }

  // OnClick on lyst element
  toogleCompletion(id) {
    const updatedTodos = this.state.todos.map(todo => {
      if (todo.id === id) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    });

    this.setState({ todos: updatedTodos });
  }

  // Rendering
  render() {
    const todos = this.state.todos.map(todo => {
      return (
        <Todo
          task={todo.task}
          completed={todo.completed}
          key={todo.id}
          id={todo.id}
          removeTodo={this.remove}
          updateTodo={this.update}
          toogleTodo={this.toogleCompletion}
        />
      );
    });

    return (
      <div className="TodoList">
        <h1>
          Todo List! <span>A Simple React Todo List App.</span>
        </h1>

        <ul>{todos}</ul>

        <NewTodoForm createTodo={this.create} />
      </div>
    );
  }
}

export default TodoList;
